@Products = new Mongo.Collection "products"

schema = new SimpleSchema
  title:
    type: String
  price:
    type: Number
  description:
    type: String
    optional: true
  createdAt:
    type: Date
    optional: true
    autoValue: ->
      new Date() unless @isSet

@Products.attachSchema schema

@Products.allow
  insert: (userId, doc) ->
    userId and doc.userId is userId
    true
  update: (userId, doc) ->
    userId == doc.userId
    true
  remove: (userId, doc) ->
    userId == doc
    true