FlowRouter.route '/',
  name: 'home'
  action: ->
    ReactLayout.render MasterLayout, 
      content: <Home />
      title: 'Frontpage'

FlowRouter.route '/products/:id',
  name: 'products_show'
  action: (params) ->
    ReactLayout.render MasterLayout,
      content: <ProductsShow id={params.id} />
      title: 'Show product'

FlowRouter.route '/products/:id/edit',
  name: 'products_edit'
  action: (params) ->
    ReactLayout.render MasterLayout,
      content: 
        <div className="col-md-6">
          <ProductForm productId={params.id} />
        </div>
      title: 'Edit item'

FlowRouter.route '/forhandlere',
  name: 'dealers'
  action: ->
    ReactLayout.render MasterLayout, 
      content: <Home />
      title: 'Dealers'