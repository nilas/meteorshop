Meteor.publish "products", ->
  Products.find()

Meteor.publish "product", (id) ->
  Products.find _id: id 

Meteor.publish 'images', ->
  Images.find()