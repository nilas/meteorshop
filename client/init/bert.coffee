Meteor.startup ->
  Bert.defaults = 
    hideDelay: 3500
    style: 'growl-top-right'
    type: 'default'