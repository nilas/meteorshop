@Header = React.createClass
  render: ->
    <div className="header">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>{@props.title}</h1>
          </div>
        </div>
      </div>
    </div>