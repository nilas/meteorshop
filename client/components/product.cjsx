@Product = React.createClass
  deleteProduct: ->
    Products.remove({_id: @props.product._id})

  render: ->
    <div className="thumbnail">
      <div className="caption">
        <h3>{@props.product.title}</h3>
        <p>{@props.product.description}</p>
        <p>
          <button className="btn btn-danger" onClick={@deleteProduct}>Remove</button>
          <a href={FlowRouter.path('products_edit', id: @props.product._id)} className="btn btn-success">Edit</a>
        </p>
      </div>
    </div>