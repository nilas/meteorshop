@ProductsShow = React.createClass
  mixins: [ReactMeteorData]

  getMeteorData: ->
    productId = @props.id

    Meteor.subscribe('product', productId)

    product: Products.findOne({_id: productId})
  
  render: ->
    if @data.product?
    
      <div>
        <h3>{@data.product.title}</h3>
        <p>{@data.product.description}</p>
      </div>
    
    else
      <Loading />