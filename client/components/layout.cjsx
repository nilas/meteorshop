@MasterLayout = React.createClass
  render: ->
    <div className="main_content">
      
      <Navbar />
      
      <Header title={@props.title} />
      
      <div className="container">
        <div className="col-md-12">
          {@props.content}
        </div>
      </div>

      <Footer />
    
    </div>