@ProductForm = React.createClass
  mixins: [ReactMeteorData, React.addons.LinkedStateMixin]

  getInitialState: ->
    errors: [], title: '', price: '', description: ''

  getMeteorData: ->
    self = @
    productId = @props.productId
    if productId
      Meteor.subscribe 'product', productId, ->
        p = Products.findOne({_id: productId })
        self.setState title: p.title, price: p.price, description: p.description
      
      product: Products.findOne({_id: productId })
    else
      product: null

  handleSubmit: (e) ->
    e.preventDefault()
    self = @ 
    data = title: @state.title, price: @state.price, description: @state.description

    operationCallback = (error, success) ->
      if success
        self.setState errors: []
        Bert.alert 'Yees, its done!', 'success'
        self.setState title: '', price: '', description: ''
      else
        self.setState errors: error.invalidKeys.map (k) => k.name
    
    if @props.productId # Update if passed an ID
      Products.update @props.productId, $set: data, operationCallback
    else
      Products.insert data, operationCallback


  errorClass: (field, classes = []) ->
    classes.push 'has-error' if @state.errors.indexOf(field) > -1
    classes.join(' ')


  render: ->
    <div className="productForm">

      <form id="productForm" className="form" onSubmit={@handleSubmit} >

        <div className={@errorClass('title', ['form-group'])}>
          <input className="form-control" type="text" name="title" placeholder="Titel" valueLink={@linkState('title')} />
        </div>

        <div className={@errorClass('price', ['form-group'])}>
          <input className="form-control" type="number" name="price" placeholder="Pris" valueLink={@linkState('price')} />
        </div>
        
        <div className={@errorClass('description', ['form-group'])} >
          <textarea id="description" className="form-control" name="description" placeholder="Beskrivelse" valueLink={@linkState('description')} ></textarea>
        </div>
        <FileField />
        <button type="submit" className="btn btn-success">Gem</button>

      </form>
    </div>