@Home = React.createClass
  mixins: [ReactMeteorData]

  componentDidMount: ->
    comp = @
    Meteor.setInterval ( ->
          @setState currentTime: new Date().getTime() if @isMounted()
        ).bind(this), 1000

  getInitialState: ->
    startTime: new Date().getTime()
    currentTime: new Date().getTime()

  getMeteorData: ->
    Meteor.subscribe('products')
    products: Products.find().fetch()

  calcTime: ->
    ms = (@state.currentTime - @state.startTime)

    secs =  ms / 1000
    ms = Math.floor(ms % 1000)
    
    minutes = secs / 60
    secs = Math.floor(secs % 60)
    hours = minutes / 60
    minutes = Math.floor(minutes % 60)
    hours = Math.floor(hours % 24)
    
    hours + ":" + minutes + ":" + secs


  render: ->
    unless @data.products?
      <Loading />
    else

      <div className="home">
        
        <div className="row">
          <div className="col-md-6">
            <h3>Tilføj produkt {@calcTime()}</h3>
            <ProductForm />
          </div>
        </div>

        <hr />

        <div className="row">
          {@data.products.map (product, index) ->
            <div className="col-md-3" key={index}><Product product={product} /></div>}
        </div>

      </div>