@Footer = React.createClass
  render: ->
    <footer className="footer">
      <div className="container">
        <div className="row">
          
          <div className="col-md-8 brand">
            Webkram - Østergade 5, 1. 8
          </div>

          <div className="col-md-4 social">
            <ul className="list list-inline pull-right">
              <li><a href="#">Kontakt</a></li>
              <li><a href="#">Facebook</a></li>
            </ul>
          </div>
        
        </div>
      </div>
    </footer>