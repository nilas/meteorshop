@Navbar = React.createClass
  activeClass: (route) ->
    'active' if ActiveRoute.name(route)

  render: ->
    <nav className="navbar navbar-default">
      <div className="container">
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>

          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              <li className={@activeClass('home')}><a href="/">koncept</a></li>
              <li className={@activeClass('dealers')}><a href="/forhandlere">forhandlere</a></li>
              <li className={@activeClass('terms')}><a href="/forretningsbetingelser">forretningsbetingelser</a></li>
              <li className={@activeClass('order')}><a href="/bestil">bestil</a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>